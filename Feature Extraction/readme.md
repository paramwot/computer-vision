# Task:-Find similar object from two images using opencv only
***
* This repository contains one python file, which is main.py
* There is `ExtractFeatures` class  
* In `brute_force_matcher` method, used "Brute-Force Matcher" (with SIFT Descriptors and Ratio Test), which takes two images path as a params. 
  
  * Brute-Force matcher is simple. It takes the descriptor of one feature in first set and is matched with all other features in second set using some distance calculation. And the closest one is returned.
* In `flann_matcher` method, used "FLANN based Matcher
"
  * FLANN stands for Fast Library for Approximate Nearest Neighbors. It contains a collection of algorithms optimized for fast nearest neighbor search in large datasets and for high dimensional features. It works faster than BFMatcher for large datasets. We will see the second example with FLANN based matcher.

***
* taking below two images for `brute_force_matcher` method:
  
![input1](input1.jpg)

* output as extracted matched features from both images

![result of Brute Force Matcher](result_brute_force.png)
***
* taking below two images for `flann_matcher` method:

![input2](input2.png)

* output as extracted matched features from both images

![result of FLANN matcher](result_FLANN.png)

***
### _Key observation_:

* BFMatcher is going to try all the possibilities (which is the meaning of "Brute Force" and hence it will find the best matches.

* FLANN, meaning "Fast Library for Approximate Nearest Neighbors", will be much faster but will find an approximate nearest neighbors. It will find a good matching, but not necessarily the best possible one.

BFMatcher | FLANN
------ | --------
slow | fast
high precision | low precision
good with few image set | performas best with large dataset


### Which is best ? BFMatcher or FLANN ?
* neither of both 
## RANSAC : 
* RANSAC is an algorithm used to robustly fit to the matched keypoints to a mathematical model of the transformation from one image to the other, for example, a homography. 
* So good matches which provide correct estimation are called inliers and remaining are called outliers. `cv.findHomography()` returns a mask which specifies the inlier and outlier points.
* RANSAC is randomly generated pairs of an image and it generate line and the points `distance<threshold` then it consider that inlier point otherwise outlier points. Iterative process and find which have maximum points it consider those all points as inlier points.

* taking below two images for `ransac_matcher` method:

![input2](input2.png)

* output as extracted matched features from both images

![result of RANSAC matcher](RANSAC_matcher_result.png)
