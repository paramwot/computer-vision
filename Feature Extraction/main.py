#required libraries
import cv2 as cv
import matplotlib.pyplot as plt

class ExtractFeatures:

    def __init__(self, ):
        super().__init__()

    def brute_force_matcher(self, image1_path, image2_path):
        image_1 = cv.imread(image1_path)
        image_2 = cv.imread(image2_path)
        # Initiate SIFT detector
        sift = cv.SIFT_create()
        # find the keypoints and descriptors with SIFT
        keypoint_img_1, descriptors_img_1 = sift.detectAndCompute(image_1, None)
        keypoint_img_2, descriptors_img_2 = sift.detectAndCompute(image_2, None)
        # BFMatcher with default params
        bf_matcher = cv.BFMatcher()
        matches = bf_matcher.knnMatch(descriptors_img_1, descriptors_img_2, k=2)
        # Apply ratio test
        features = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                features.append([m])
        # cv.drawMatchesKnn expects list of lists as matches.
        result = cv.drawMatchesKnn(image_1, keypoint_img_1, image_2, keypoint_img_2, features, None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
        cv.imwrite('result_brute_force.png', result)
        plt.imshow(result), plt.show()

    def flann_matcher(self, image_1_path, image_2_path):
        '''FLANN Matcher'''
        img1 = cv.imread(image_1_path)
        img2 = cv.imread(image_2_path)
        sift = cv.SIFT_create()
        # find the keypoints and descriptors with SIFT
        keypoint_1, descriptors_1 = sift.detectAndCompute(img1, None)
        keypoint_2, descriptors_2 = sift.detectAndCompute(img2, None)
        # FLANN parameters
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)  # or pass empty dictionary
        flann = cv.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(descriptors_1, descriptors_2, k=2)
        # Need to draw only good matches, so create a mask
        matchesMask = [[0, 0] for i in range(len(matches))]
        # ratio test as per Lowe's paper
        for i, (m, n) in enumerate(matches):
            if m.distance < 0.7 * n.distance:
                matchesMask[i] = [1, 0]
        draw_params = dict(matchColor=(0, 255, 0),
                           singlePointColor=(255, 0, 0),
                           matchesMask=matchesMask,
                           flags=cv.DrawMatchesFlags_DEFAULT)
        img3 = cv.drawMatchesKnn(img1, keypoint_1, img2, keypoint_2, matches, None, **draw_params)
        cv.imwrite('result_FLANN.png', img3)
        plt.imshow(img3, ), plt.show()

    def ransac_matcher(self, image_1_path, image_2_path):
        '''RANSAC'''
        MIN_MATCH_COUNT = 10
        img1 = cv.imread(image_1_path)  # queryImage
        img2 = cv.imread(image_2_path)  # trainImage
        # Initiate SIFT detector
        sift = cv.SIFT_create()
        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)
        flann = cv.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(des1, des2, k=2)
        # store all the good matches as per Lowe's ratio test.
        good = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                good.append(m)
        if len(good) > MIN_MATCH_COUNT:
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
            M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC, 5.0)
            matchesMask = mask.ravel().tolist()
            h, w, d = img1.shape
            pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv.perspectiveTransform(pts, M)
            img2 = cv.polylines(img2, [np.int32(dst)], True, 255, 3, cv.LINE_AA)
        else:
            print("Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
            matchesMask = None

        draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                           singlePointColor=None,
                           matchesMask=matchesMask,  # draw only inliers
                           flags=2)
        img3 = cv.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)
        cv.imwrite('RANSAC_matcher_result.png', img3)
        plt.imshow(img3, 'gray'), plt.show()

if __name__ == '__main__':
    import numpy as np
    instance = ExtractFeatures()
    instance.brute_force_matcher("param.png", "pt.jpg")
    instance.flann_matcher("laptop.jpeg", 'table.jpeg')
    instance.ransac_matcher("laptop.jpeg", 'table.jpeg')