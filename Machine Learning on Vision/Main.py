# Required Libraries
import math
import cv2 as cv
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import numpy as np
import matplotlib.pyplot as plt


class KmeansClustering:
    def __init__(self):
        super().__init__()

    def calc_distance(self, x, y, a, b, c):
        distance = abs((a * x + b * y + c) / (math.sqrt((a * a + b * b))))
        return distance

    def elbow_method(self, image_path):

        # List for distortion
        distortions = []
        # List for Inertias
        inertias = []
        mapping1 = {}
        mapping2 = {}
        # Range for k values
        K = range(1, 10)

        # Reading image
        img = cv.imread(image_path)
        # Resizing to two dimentional array
        two_dim_image = img.reshape((-1, 3))

        for k in K:
            # Building and fitting the model
            kmeanModel = KMeans(n_clusters=k).fit(two_dim_image)
            kmeanModel.fit(two_dim_image)

            distortions.append(sum(np.min(cdist(two_dim_image, kmeanModel.cluster_centers_,
                                                'euclidean'), axis=1)) / two_dim_image.shape[0])
            inertias.append(kmeanModel.inertia_)

            mapping1[k] = sum(np.min(cdist(two_dim_image, kmeanModel.cluster_centers_,
                                           'euclidean'), axis=1)) / two_dim_image.shape[0]
            mapping2[k] = kmeanModel.inertia_

        # Prints the value of k and respective distortion
        # for key, val in mapping1.items():
        #     print(f'{key} : {val}')

        # Plots the k and distortion Graph
        # plt.plot(K, distortions, 'bx-')
        # plt.xlabel('Values of K')
        # plt.ylabel('Distortion')
        # plt.title('The Elbow Method using Distortion')
        # plt.show()

        # Below plot draws the line connecting first and last point of distortion
        plt.plot(K, distortions, 'bx-')
        plt.plot([K[0], K[-1]], [distortions[0], distortions[-1]], 'ro-'), plt.show()
        plt.xlabel('Values of K')
        plt.ylabel('Distortion')
        plt.title('The Elbow Method using Distortion')
        plt.show()

        a = distortions[0] - distortions[-1]
        b = K[-1] - K[0]
        c1 = K[0] * distortions[-1]
        c2 = K[8] * distortions[0]
        c = c1 - c2

        distance_of_points_from_line = []
        for k in range(9):
            distance_of_points_from_line.append(self.calc_distance(K[k], distortions[k], a, b, c))

        optimum_value_of_k = distance_of_points_from_line.index(max(distance_of_points_from_line)) + 1

        return optimum_value_of_k

    def get_color_cluster_image(self, image_path):
        # Reading Image
        img = cv.imread(image_path)
        # Reshaping from 3 dimentional to 2 dimentional array
        two_dim_image = img.reshape((-1,3))
        # convert to np.float32
        reshaped_img = np.float32(two_dim_image)
        # define criteria, number of clusters(K) and apply kmeans()
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        # Number of times the algorithm is executed using different initial labellings
        attempt = 10
        # K value
        K = self.elbow_method(image_path)

        ret,label,center=cv.kmeans(reshaped_img,K,None,criteria, attempt ,cv.KMEANS_RANDOM_CENTERS)
        # Now convert back into uint8, and make original image
        center = np.uint8(center)
        res = center[label.flatten()]

        res2 = res.reshape((img.shape))
        cv.imwrite('clusterd_output.png', res2)
        # cv.imshow('result',res2)
        cv.waitKey(0)
        cv.destroyAllWindows()


if __name__ == '__main__':
    instance = KmeansClustering()
    instance.get_color_cluster_image('cropped.png')