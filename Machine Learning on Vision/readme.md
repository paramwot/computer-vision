# Task: Write an script to convert given image into n color cluster

######  This repository contains one python file (Main.py)

***
* There is `elbow_method` function, which returns optimum value of k based on image
* `get_color_cluster_image` function returns n color clustered image 

***
* Below is the input image
![input_image](cropped.png)
  
* Below is the graph for K Vs. Distortion
![distortion graph](distortion_graph.png)
  
* output image
![output](clusterd_output.png)
  
### Key challenge: 
* To decide optimum value of k, we need to plot elbow method
* ploting elbow method was easy but to take k from it programmatically was bit challenging
* Found the best approach: 
    * drawn a linear line connecting first and last point of distortion
      ![distortion and linear line](distortion.png)
    * calculated euclidean distance between distortion line and linear line
    * higher the distance value -> higher the change -> respective k value for it is `optimum value`