## Tasks:
1. Linear Filter (blur, sharpen, edge detect and contour)
2. Median Filter, Bilateral Filter, Morphology
3. Image Blending (crop one image in circle shape and put inside another)
4. Template matching

## This repository contains two python scipts: 
  * functions.py for all the functions
  * Main.py is the main file to run respective functions

### How to run? 
* just un-comment any function which you want,
* you can read function doc for what function does

***

### Results
* Image Blurring
![blur_image](bluterd.png)
  
* Image sharpen
![sharpen_image](sharpen_image.png)

* Drawing contour
![contour_drawing](contour_draw.png)

* Median Filter
![median filter](median_filter.png)

* Bileteral Filtering
![bileteral filter](bilateral_filter.png)

* morphology
![morphology](morphology.png)