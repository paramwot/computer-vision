import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageChops, ImageDraw

class LinearFilter:
    def __init__(self, path_to_image ):
        super(LinearFilter, self).__init__()
        self.path_to_image = path_to_image
        self.img = cv.imread(self.path_to_image)

    def dispaly_img_window(self, name, image):
        cv.imshow(name, image)
        cv.waitKey(0)
        cv.destroyAllWindows()

    def read_image(self):
        '''this function reads the image'''
        self.dispaly_img_window(str(self.path_to_image), self.img)

    def blur_image(self):
        '''this function blurs the image'''
        Gaussian_blur_img = cv.GaussianBlur(self.img, (7, 7), 0)
        image_window = np.concatenate((self.img, Gaussian_blur_img), axis=1)
        cv.imwrite("bluterd.png", image_window)
        self.dispaly_img_window("Blur Image", image_window)

    def sharpen_image(self):
        '''this function sharpen the image'''
        kernel = np.array([[-1, -1, -1],
                           [-1, 9, -1],
                           [-1, -1, -1]])
        sharpened = cv.filter2D(self.img, -1, kernel)  # applying the sharpening kernel to the input image & displaying it.
        image_window = np.concatenate((self.img, sharpened), axis=1)
        cv.imwrite("sharpen_image.png", image_window)
        self.dispaly_img_window("Sharpen Image", image_window)

    def edge_detection(self):
        '''this function detectes the edge using Canny edge detector'''
        gray_img = cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)
        edges_img = cv.Canny(gray_img, 100, 100)
        self.dispaly_img_window(str(self.path_to_image), self.img)
        cv.imwrite("edge_detection.png", edges_img)
        self.dispaly_img_window("Edge Detection", edges_img)

    def contours_draw(self):
        '''this function draws the contour'''
        imgray = cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)
        ret, thresh = cv.threshold(imgray, 127, 255, 0)
        (cnts, _) = cv.findContours(thresh.copy(), cv.RETR_TREE,
                                     cv.CHAIN_APPROX_SIMPLE)
        contour_image = cv.drawContours(self.img, cnts, -1, (0, 255, 0), 3)
        image_window = np.concatenate((self.img, contour_image), axis=1)
        cv.imwrite("contour_draw.png", image_window)
        self.dispaly_img_window("Draw Contour", image_window)

    def median_filter(self):
        '''this function applies the median filter'''
        median_filter_img = cv.medianBlur(self.img, 5)
        image_window = np.concatenate((self.img, median_filter_img), axis=1)
        cv.imwrite("median_filter.png", image_window)
        self.dispaly_img_window("Median Filter", image_window)

    def bilateral_filter(self):
        '''this function applies the bilateral filter'''
        bilateral_img = cv.bilateralFilter(self.img, 9, 75, 75)
        image_window = np.concatenate((self.img, bilateral_img), axis=1)
        cv.imwrite("bilateral_filter.png", image_window)
        self.dispaly_img_window("Bilateral Filtering", image_window)

    def morphology(self):
        '''this function applies morphology'''
        kernel = np.ones((5, 5), np.uint8)
        img_erosion = cv.erode(self.img, kernel, iterations=1)
        img_dilation = cv.dilate(self.img, kernel, iterations=1)
        image_window = np.concatenate((self.img, img_erosion, img_dilation), axis=1)
        cv.imwrite("morphology.png", image_window)
        self.dispaly_img_window("Morphology", image_window)

    def template_matching(self, template_image):
        '''this funtion matches the tamplate'''
        img = cv.imread('param.png')
        img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        template = cv.imread(template_image, 0)

        height, width = template.shape[::]

        res = cv.matchTemplate(img_gray, template, cv.TM_SQDIFF)
        plt.imshow(res, cmap='gray')

        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)

        top_left = min_loc
        bottom_right = (top_left[0] + width, top_left[1] + height)
        cv.rectangle(img, top_left, bottom_right, (255, 0, 0), 2)

        image_window = np.concatenate((self.img, img), axis=1)
        cv.imwrite("template_matching.png", image_window)
        self.dispaly_img_window("Teplate Matching", image_window)

    def crop_to_circle(self, im):
        '''function which crops image into circular'''
        bigsize = (im.size[0] * 3, im.size[1] * 3)
        mask = Image.new('L', bigsize, 0)
        ImageDraw.Draw(mask).ellipse((0, 0) + bigsize, fill=255)
        mask = mask.resize(im.size, Image.ANTIALIAS)
        mask = ImageChops.darker(mask, im.split()[-1])
        im.putalpha(mask)

    def blending_image(self, img_to_crop, other_image):
        '''Blending Image'''
        img = Image.open(img_to_crop).convert('RGBA')
        self.crop_to_circle(img)
        img.save('cropped.png')
        first_img = cv.imread('cropped.png', 1)
        second_img = cv.imread(other_image, 1)
        first_img = cv.resize(first_img, (300, 279))
        second_img = cv.resize(second_img, (300, 279))
        result = cv.addWeighted(first_img, 0.3, second_img, 0.7, 0)
        self.dispaly_img_window("First Image", first_img)
        self.dispaly_img_window("second Image", second_img)
        # cv.imwrite("blending_image.png", result)
        self.dispaly_img_window("Blending result", result)