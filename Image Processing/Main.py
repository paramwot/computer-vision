from functions import *

def main():
    instance = LinearFilter('param.png')
    # instance.read_image()
    # instance.blur_image()
    # instance.sharpen_image()
    # instance.contours_draw()
    # instance.median_filter()
    # instance.bilateral_filter()
    instance.morphology()
    # instance.template_matching('param_cropped.png')
    # instance.blending_image('pt.jpg', 'param_cropped.png')
    # print(instance.template_matching.__doc__)

if __name__ == '__main__':
    main()